const randomNoises = [
    "[SCREAMING]",
    '"Why are you here, Gandalf?"',
    "[Bowling alley noises]",
    '"OOK OOK OOK"',
    "[Maniacal laughter]",
    '"Ha ha ha, what a story Mark!"',
    "[Chestburster sounds]",
    "[Chainsaw revving]",
    '"Mom, more food please... MOM!"',
    '"Beep boop son... Beep boop"'
];
const addTextDelay = 500;

const addText = (paragraph) => {
    const textAddition = document.createElement("span");
    const rng = Math.floor(Math.random() * 100);
    textAddition.textContent = rng < 80 ? "TÖMPS TÖMPS " :  randomNoises[Math.floor(Math.random() * randomNoises.length)] + " ";
    textAddition.style.fontSize = rng + "px";
    paragraph.appendChild(textAddition);
}

const runSimulator = () => {
    window.document.getElementById("startButton").remove();
    const textElement = window.document.getElementById("paragraph");
    const interval = setInterval(addText, addTextDelay, textElement);

    const background = document.getElementById("bg");
    background.style.backgroundImage = "url('./background.jpg')";

    const audioPlayer = document.getElementById("audioPlayer");
    audioPlayer.volume = 0.1;
    audioPlayer.play();
}

document.getElementById("startButton").addEventListener("click", runSimulator);