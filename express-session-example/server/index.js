import express from 'express';
import session from 'express-session';
import cors from 'cors';
import axios from 'axios';

const app = express();
const PORT = 3000;
const axiosInstance = axios.create({
    baseURL: 'http://localhost:3001/'
  });
app.use(express.json());
app.use(cors({
    methods: ['POST', 'PUT', 'GET', 'OPTIONS', 'HEAD'],
    credentials: true,
    origin: true
  })
);
app.set('trust proxy', 1);
app.use(session({
  secret: 'super secret key',
  resave: false,
  saveUninitialized: true,
  cookie: { maxAge: 3600000, secure: true, }, // Do NOT use sameSite: 'none' on production environments
  httpOnly: true    
}));

app.get('/api/settings', (req, res) => {
    // You could create a middleware that makes this check and prevents the access to pages that require a log in, a certain role etc.
    if (req.session.user) {
        axiosInstance.get('/settings', {params: { userId: req.session.user.id}})
        .then((response) => {
            const data = response.data;
            res.send(data);
            return;
        })
        .catch((error) => {
            console.log(error);
            res.status(500).send(error);
        })
    }
    res.status(401).send("Get out of here stalker!");
});

app.get('/api/profile', (req, res) => {
    const user = req.session.user;
    console.log(user);
    if (user) {
        res.json({
            username: user.username,
            fullName: user.fullName,
            id: user.id
        });
        return;
    }
    res.status(401).send("Get out of here stalker!");
});

app.post('/api/login', function (req, res) {
    const {username, password} = req.body;
    axiosInstance.get('/users', {params: { username: username, password: password}}) // Some Vastaamo level security here
    .then((response) => {
        const data = response.data;
        if (data.length === 0) {
            res.status(401).send("Wrong username or password");
            return;
        }
        req.session.user = data[0]; // json-server returns an array so we'll use the first user that matched our query in our session
        console.log(req.session.user);
        res.send("ok");
    })
    .catch((error) => {
        console.log(error);
        res.status(500).send(error);
    })
});

app.listen(PORT, () => {
  console.log(`Server listening to port ${PORT}`);
});