import { useState } from 'react'
import './App.css'
import axios from 'axios';

function App() {
  const [currentUser, setCurrentUser] = useState(null);
  const [inputData, setInputData] = useState({
    username: "",
    password: ""
  });

  const handleChange = (event) => {
    const updatedInputs = {...inputData, [event.target.name]: event.target.value};
    setInputData(updatedInputs);
    console.log(inputData);
  }
  const handleLogin = (event) => {
    event.preventDefault();
    axios.post('http://localhost:3000/api/login', inputData, {
      headers: {
        'Content-Type': 'application/json'
      },
      withCredentials: true
    })
    .then(response => {
      if (response.data) {
        setCurrentUser(response.data);
        alert("Welcome, " + response.data.username + "!");
      }
    }).catch(error => {
      if (error.response) {
        // Prints the message the server sent if response status was something else than 2XX
        alert(error.response.data);
      } else {
        alert(error);
      }
    })
  }

  return (
    <>
      <h1>Super Secure Login App</h1>
      <div className="card">
        <input name="username" value={inputData.username} onChange={handleChange} placeholder="Username"/>
        <input name="password" value={inputData.password} onChange={handleChange} placeholder="Password"/>
        <button onClick={handleLogin}>
          LOG IN!!!!
        </button>
        <p>
          
        </p>
      </div>
    </>
  )
}

export default App
