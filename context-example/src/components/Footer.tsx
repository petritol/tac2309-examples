import { useContext } from "react";
import UserContext from "../contexts/UserContext";
import ThemeContext from "../contexts/ThemeContext";

const Footer = () => {
  const { currentUser } = useContext(UserContext);
  const { theme } = useContext(ThemeContext);
  console.log("Footer rendered");
  return (
    <div className="container bg-light">
      <footer className="d-flex flex-wrap justify-content-between align-items-center py-3 border-top">
        <div className="col d-flex align-items-center">
          <a
            href="/"
            className="mb-3 me-2 mb-md-0 text-body-secondary text-decoration-none lh-1"
          ></a>
          <span className="mb-3 mb-md-0 text-body-secondary">
            &copy; 2023 Weyland-Yutani, Inc. We{" "}
            {theme === "light" ? "own" : "❤️"} you.{" "}
            {currentUser && (
              <a
                href={
                  "https://www.spam.com/subscribe?email=" + currentUser.email
                }
              >
                Subscribe to newsletter
              </a>
            )}
          </span>
        </div>
      </footer>
    </div>
  );
};

export default Footer;
