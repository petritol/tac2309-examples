import SearchQueryContext from "../contexts/SearchQueryContext";
import { useContext } from "react";

const SearchResults = () => {
  const { searchQuery } = useContext(SearchQueryContext);
  console.log("SearchResults rendered");
  return (
    <div className="row text-centered">
      {searchQuery && <p>Found 0 results for "{searchQuery}"!</p>}
    </div>
  );
};

export default SearchResults;
