import { useContext } from "react";
import ThemeContext from "../contexts/ThemeContext";
import SearchField from "./SearchField";
import SearchResults from "./SearchResults";
import UserContext from "../contexts/UserContext";

const MainContainer = () => {
  console.log("MainContainer rendered");
  const { theme } = useContext(ThemeContext);
  const { currentUser } = useContext(UserContext);
  return (
    <div className={"container main-container bg-" + theme}>
      <div className="row text-center pt-5">
        <h1 className="fw-bold text-uppercase logo-text">
          {theme === "light" ? "Poople" : "✨Coodle✨"}
        </h1>
        <p className="fs-3">
          We're {theme === "light" ? "💩" : "🦄"} and we know it
        </p>
        <div className="row text-start ">
          {currentUser && (
            <>
              <p>
                {theme === "light" && "Welcome back, "}
                {theme === "cute" && "Namaste, "}
                {currentUser.username}!<br />
                Your last login was on {currentUser.lastLogin}
              </p>
            </>
          )}
        </div>
        <div className="row d-flex justify-content-center ">
          <SearchField />
          <SearchResults />
        </div>
      </div>
    </div>
  );
};
export default MainContainer;
