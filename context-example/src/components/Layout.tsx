import NavBar from "./NavBar";
import MainContainer from "./MainContainer";
import Footer from "./Footer";
import { useContext } from "react";
import ThemeContext from "../contexts/ThemeContext";

const Layout = () => {
  console.log("Layout rendered");
  const { theme } = useContext(ThemeContext);
  return (
    <div className={"background-image background-image-" + theme}>
      <NavBar />
      <MainContainer />
      <Footer />
    </div>
  );
};

export default Layout;
