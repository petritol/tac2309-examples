import { useContext } from "react";
import ThemeContext from "../contexts/ThemeContext";
import UserContext from "../contexts/UserContext";
import type { UserType } from "../contexts/UserContext";

const NavBar = () => {
  console.log("Navbar rendered");
  const { theme, toggleTheme } = useContext(ThemeContext);
  const { currentUser, setCurrentUser } = useContext(UserContext);

  const login = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault();
    const user: UserType = {
      id: 1,
      username: "petritolonen666",
      lastLogin: Date().toString(),
      role: "user",
      email: "petri@spam.me",
    };
    setCurrentUser(user);
  };
  return (
    <nav className="navbar navbar-expand-lg navbar-fixed-top navbar-light bg-light">
      <div className="container">
        <a className="navbar-brand" href="#">
          {theme === "light" ? "Poople" : "Coodle >:3"}
        </a>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarNavAltMarkup"
          aria-controls="navbarNavAltMarkup"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div
          className="collapse navbar-collapse justify-content-end"
          id="navbarNavAltMarkup"
        >
          <div className="navbar-nav">
            <a className="nav-link active" aria-current="page" href="#">
              Home
            </a>
            <a className="nav-link" href="#">
              Images
            </a>
            <a className="nav-link" href="#">
              Videos
            </a>
            <a className="nav-link" href="#">
              Map
            </a>
            <a className="nav-link" href="#">
              Donate!!
            </a>
            {currentUser && currentUser.role === "admin" && (
              <a className="nav-link" href="#">
                <b>Super Secret Admin Menu</b>
              </a>
            )}
            <form className="d-flex">
              <button onClick={toggleTheme} className="btn btn-dark mx-1">
                Toggle theme
              </button>
              {!currentUser && (
                <button onClick={login} className="btn btn-dark mx-1">
                  Log in!
                </button>
              )}
            </form>
            {currentUser && (
              <a className="nav-link" href="#">
                {currentUser.username}
                {theme === "light" ? "" : "-kun :3"}
              </a>
            )}
          </div>
        </div>
      </div>
    </nav>
  );
};

export default NavBar;
