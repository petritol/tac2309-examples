import { useState } from "react";
import Layout from "./Layout";
import ThemeContext from "../contexts/ThemeContext";
import UserContext from "../contexts/UserContext";
import type { UserType } from "../contexts/UserContext";
import SearchQueryContext from "../contexts/SearchQueryContext";

const App = () => {
  console.log("App rendered");
  const [theme, setTheme] = useState("light");
  const [currentUser, setCurrentUser] = useState<UserType | null>(null);
  const [searchQuery, setSearchQuery] = useState("");
  const toggleTheme = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault();
    setTheme((prev) => (prev === "light" ? "cute" : "light"));
  };

  return (
    <ThemeContext.Provider value={{ theme: theme, toggleTheme: toggleTheme }}>
      <UserContext.Provider
        value={{ currentUser: currentUser, setCurrentUser: setCurrentUser }}
      >
        <SearchQueryContext.Provider
          value={{
            searchQuery: searchQuery,
            setSearchQuery: setSearchQuery,
          }}
        >
          <Layout />
        </SearchQueryContext.Provider>
      </UserContext.Provider>
    </ThemeContext.Provider>
  );
};
export default App;
