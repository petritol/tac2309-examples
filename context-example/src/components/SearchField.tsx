import SearchQueryContext from "../contexts/SearchQueryContext";
import { useContext } from "react";

const SearchField = () => {
  const { searchQuery, setSearchQuery } = useContext(SearchQueryContext);
  console.log("SearchField rendered");
  return (
    <div className="mb-3 col-6">
      <input
        type="text"
        className="form-control"
        placeholder={"Search for something!"}
        value={searchQuery}
        onChange={(e) => setSearchQuery(e.target.value)}
      />
    </div>
  );
};

export default SearchField;
