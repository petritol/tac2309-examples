import { createContext } from "react";

type SearchQueryType = {
  searchQuery: string;
  setSearchQuery: React.Dispatch<React.SetStateAction<string>>;
};

const SearchQueryContext = createContext<SearchQueryType>(
  {} as SearchQueryType // Basically means: Everything is okay as long as we use the context inside a context provider
);

export default SearchQueryContext;
