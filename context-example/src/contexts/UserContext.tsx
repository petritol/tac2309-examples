import { createContext } from "react";

type UserType = {
  id: number;
  username: string;
  lastLogin: string;
  role: string;
  email: string;
};
type UserContextType = {
  currentUser: UserType | null;
  setCurrentUser: React.Dispatch<React.SetStateAction<UserType | null>>;
};

const UserContext = createContext<UserContextType>({} as UserContextType); // Basically means: Everything is okay as long as we use the context inside a context provider

export default UserContext;
export type { UserType };
