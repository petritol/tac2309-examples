import { createContext } from "react";
type ThemeContextType = {
  theme: string;
  toggleTheme: (
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => void | undefined;
};

const ThemeContext = createContext<ThemeContextType>({} as ThemeContextType); // Basically means: Everything is okay as long as we use the context inside a context provider

export default ThemeContext;
